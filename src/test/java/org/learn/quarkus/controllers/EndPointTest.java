package org.learn.quarkus.controllers;

import groovyjarjarantlr4.runtime.misc.IntArray;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.time.*;

import static org.junit.jupiter.api.Assertions.*;

class EndPointTest {
    @Test
    void getAgeFromInstant() {
//        ZoneId zoneId = ZoneId.of("+7");
//        Instant dob = Instant.ofEpochMilli(793213200000L);
//        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(dob, zoneId);
////        int age = dob.compareTo(Instant.now());
////        System.out.println(LocalDate.from(dob).getYear());
//        System.out.println(zonedDateTime.getYear());
//        System.out.println(zonedDateTime.getMonthValue());
//        System.out.println(zonedDateTime.getDayOfMonth());
//        ZonedDateTime now = ZonedDateTime.now(zoneId);

        LocalDate date = LocalDate.ofInstant(Instant.ofEpochMilli(793213200000L), ZoneId.of("+7"));
        Period age = Period.between(date, LocalDate.now(ZoneId.of("+7")));

//        Period age = Period.between(zonedDateTime.toLocalDate(), now.toLocalDate());
        System.out.println(String.format("%d tahun, %d bulan, %d hari", age.getYears(),age.getMonths(),age.getDays()));
    }
}