package org.learn.quarkus.proxy;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.learn.quarkus.entities.Book;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/book")
@RegisterRestClient(configKey = "book-rest-client")
public interface BookRestClient {

    @GET
    @Path("/{id}")
    Book findByBookId(@PathParam("id") Long id);
}
