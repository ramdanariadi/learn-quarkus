package org.learn.quarkus.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.List;

@Entity
public class Person extends PanacheEntity {
    @Transient
    public Integer age; // or we can use transient keyword before attribute name
    public String name;
    public String email;
    public Long createdAt;
    public Status status;

    public static enum Status{
        NONACTIVE,ACTIVE
    }

    public void setAge(){
        LocalDate date = LocalDate.ofInstant(Instant.ofEpochMilli(this.createdAt), ZoneId.of("+7"));
        Period age = Period.between(date, LocalDate.now(ZoneId.of("+7")));
        this.age = age.getYears();
    }
    public Integer getAge(){
        LocalDate date = LocalDate.ofInstant(Instant.ofEpochMilli(this.createdAt), ZoneId.of("+7"));
        Period age = Period.between(date, LocalDate.now(ZoneId.of("+7")));
        return age.getYears();
    }

    public static Person findByNameAndEmail(String name, String email){
         Person joe = find("name = ?1 and email = ?2", name, email).firstResult();
        System.out.println(joe);
        return joe;
    }

    public static List<Person> allActivePerson(){
        return list("status",Status.ACTIVE);
    }

}
