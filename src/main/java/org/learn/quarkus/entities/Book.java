package org.learn.quarkus.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Book extends PanacheEntityBase {
    @Id @GeneratedValue
    Long id;
    @NotBlank(message = "tittle tidak boleh kosong")
    public String tittle;
    public String author;
}
