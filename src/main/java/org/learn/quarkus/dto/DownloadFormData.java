package org.learn.quarkus.dto;

import org.jboss.resteasy.reactive.PartType;
import org.jboss.resteasy.reactive.RestForm;

import javax.ws.rs.core.MediaType;
import java.io.File;

public class DownloadFormData {
    @RestForm
    public String name;

    @RestForm
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    public File file;
}
