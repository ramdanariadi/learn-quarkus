package org.learn.quarkus.response;

public class Response {
    private Integer code;
    private Object data;

    public static Response ok(Object obj){
        Response response = new Response();
        response.code = 200;
        response.data = obj;
        return response;
    }
}
