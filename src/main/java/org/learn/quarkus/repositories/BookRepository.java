package org.learn.quarkus.repositories;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.learn.quarkus.entities.Book;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

@ApplicationScoped
public class BookRepository implements PanacheRepository<Book> {

    public Book findByBookId(Long id){
        return findById(id);
    }

    @Transactional
    public boolean storeBook(Book book){
        persist(book);
        return isPersistent(book);
    }
}
