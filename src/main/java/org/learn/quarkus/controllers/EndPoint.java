package org.learn.quarkus.controllers;

import io.smallrye.common.annotation.Blocking;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.resteasy.reactive.*;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;
import org.learn.quarkus.dto.Foo;
import org.learn.quarkus.dto.FormData;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Date;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Retention(RetentionPolicy.RUNTIME)
@HttpMethod("CHEESE")
@interface CHEESE{}
@Path("/rest")
public class EndPoint {

    @Path("/hello")
    @GET
    public String hello(){
        return "hello";
    }

    @Path("/cheese")
    @CHEESE
    public String cheese(){
        return "cheese";
    }

    @POST
    @Path("/allparams/{foo}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response allParams(@RestPath String foo,
                                         @RestQuery String age,
                                         @RestCookie String role,
                                         @RestMatrix String group,
                                         @RestHeader("X-secret") String xsecret,
                                         @RestForm String quote){
        Map<String, String> allParams = new HashMap<>();
        allParams.put("foo",foo);
        allParams.put("age",age);
        allParams.put("role",role);
        allParams.put("group",group);
        allParams.put("X-secret",xsecret);
        allParams.put("quote",quote);
        return Response.ok(allParams).build();
    }

    @GET
    @Path("/{name}/{age:\\d+}")
    public String nameAge(String name, int age){
        return String.format("my name is : %s and %d years old", name, age);
    }

    @ConfigProperty(name = "quarkus.http.body.uploads-directory")
    String uploadDir;

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public String uploadImage(@MultipartForm FormData formData) throws IOException, InterruptedException {
        System.out.println(formData.file.fileName());
        System.out.println(formData.file.filePath());

        // create stream
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(formData.file.filePath().toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // read all bytes
        File customDir = new File(uploadDir);
        String filename = customDir.getAbsolutePath() + File.separator + formData.file.fileName();
//        Thread.sleep(10000);
        // close stream after uploaded success
        System.out.println(filename);
        System.out.println(Paths.get(filename));

        // Paths.get(filename) will return instance of path
        try {
            byte[] bytes = fileInputStream.readAllBytes();
            Files.write(Paths.get(filename),bytes, StandardOpenOption.CREATE_NEW);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fileInputStream.close();
        }

        return uploadDir;
    }

    @GET
    @Path("/getfile")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response getFile(){
        File file = new File(Paths.get(uploadDir).toAbsolutePath().toString() + File.separator + "Random.png");
        Response.ResponseBuilder responseBuilder = Response.ok((Object) file);
        responseBuilder.header("Content-Disposition","attachment;filename=\"Rando m.png\"");
//        DownloadFormData downloadFormData = new DownloadFormData();
//        downloadFormData
        return responseBuilder.build();
//        DownloadFormData downloadFormData = new DownloadFormData();
//        downloadFormData.file = file;
//        downloadFormData.name = "Random pic";
//        return downloadFormData;
    }

    @GET
    @Path("/customproperties")
    public RestResponse<String> customResponseProperties(){
        return RestResponse.ResponseBuilder.ok("Hello with custom response properties",MediaType.TEXT_PLAIN)
                .header("foo","bar")
                .cookie(new NewCookie("fooCookie","barCookie"))
                .expires(Date.from(Instant.now().plus(Duration.ofDays(2))))
                .build();
    }

    @GET
    @Path("/testsse")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    public Multi<String> tesStreamSSE(){
        Multi<String> sse = Multi.createFrom().iterable(() -> new Iterator<String>() {
            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public String next() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                return "Foo";
            }
        });
        return sse;
    }

    @GET
    @Path("/testasync")
    public Uni<Foo> testAsyncOrReactive(){
        Uni<Foo> uni = Uni.createFrom().item(() -> {
            Foo fooTmp = new Foo();
            fooTmp.value = "successfully foo";
            double n = 2 / 0;
            Foo fnull = null;
            return fnull;
        }).onItem()
                .ifNull()
                .fail().onFailure().recoverWithItem(() -> new Foo());
        return uni;
    }

    @GET
    @Path("/getpath")
    public String getAbsolutePath(){
//        String
        File file = new File(Paths.get(uploadDir).toAbsolutePath().toString() + File.separator + "Random.png");
        return Paths.get(uploadDir).toAbsolutePath().toString() + File.separator + "Random.png";
    }

    @Blocking
    @GET
    @Path("/overrideblocking")
    public Uni<String> overrideBlockingHello() throws InterruptedException {
        Uni<String> item = Uni.createFrom().item(() -> {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("supplier finish creating item");
            return "tes tes";
        });
//        item.invoke(() -> System.out.println("call item"));
//        System.out.println("before ulala sent");
        return item;
    }

    //    @Blocking
    @GET
    @Path("/blockingonreactiveway")
    public Uni<String> blockingHello() throws InterruptedException {
        return Uni.createFrom().item("Yaaaawwwwnnnnnn…")
                // do a non-blocking sleep
                .onItem().delayIt().by(Duration.ofSeconds(10));
    }

    public static class CustomException extends RuntimeException {
        public final String name;


        public CustomException(String name) {
            this.name = name;
        }
    }

    public void testThrowCustomException() throws CustomException{
        throw new CustomException("coba exception");
    }

    @ServerExceptionMapper
    public RestResponse<String> catchCustomException(CustomException ce){
        return RestResponse.status(Response.Status.BAD_REQUEST, ce.name);
    }

    @GET
    @Path("/textcustomexception")
    public String textCustomExceptionAndConvertToResponse(){
        testThrowCustomException();
        return "";
    }



}
