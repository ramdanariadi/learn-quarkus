package org.learn.quarkus.controllers;

import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.faulttolerance.Timeout;
import org.eclipse.microprofile.openapi.annotations.headers.Header;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.ExampleObject;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBodySchema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.learn.quarkus.entities.Book;
import org.learn.quarkus.proxy.BookRestClient;
import org.learn.quarkus.repositories.BookRepository;
import org.learn.quarkus.response.Response;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.print.attribute.standard.Media;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

@Path("/book")
public class BookController {

    @Inject
    BookRepository bookRepository;

    @Inject
    Validator validator;

    @RestClient
    BookRestClient bookRestClient;

    @POST
    @RequestBody(content = @Content(schema = @Schema(implementation = Book.class), mediaType = MediaType.APPLICATION_JSON))
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response store(@Valid Book book){
        Set<ConstraintViolation<Book>> violationSet = validator.validate(book);
        violationSet.forEach(new Consumer<ConstraintViolation<Book>>() {
            @Override
            public void accept(ConstraintViolation<Book> bookConstraintViolation) {
                System.out.println(String.format("field %s %s",bookConstraintViolation.getPropertyPath(),bookConstraintViolation.getMessage()));
                System.out.println(bookConstraintViolation);
            }
        });
        book.tittle = "f";
        return Response.ok(bookRepository.storeBook(book));
    }

    private AtomicLong coutner = new AtomicLong(0);

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponses(value = {
            @APIResponse(
                    name = "success", description = "success getting book information", responseCode = "200",
                    content =
                    @Content(mediaType = MediaType.APPLICATION_JSON,
                            example = "{\"tittle\": \"book tittle\", \"author\": \"author 1\"}",
                            schema = @Schema(implementation = Book.class)
                    )
            ),
            @APIResponse(
                    name = "success", description = "not valid id", responseCode = "400",
                    content =
                    @Content(mediaType = MediaType.APPLICATION_JSON,
                            example = "{\"cause\": \"supplied id invalid\"}"
                    )
            )
    })
    public Book findById(@PathParam("id") Long id) throws InterruptedException {
        Long counterTmp = coutner.getAndIncrement();
        if(counterTmp % 3 == 0){
            System.out.println("findById called and error : "+counterTmp);
            throw new RuntimeException("error");
        }

        if(counterTmp % 5 == 0){
            System.out.println("findById called and error timeout : "+counterTmp);
            Thread.sleep(1000);
        }

        System.out.println("findById called : "+counterTmp);
        return bookRepository.findByBookId(id);
    }

    @GET
    @Path("/id/{id}") // just for demonstration
    @Produces(MediaType.APPLICATION_JSON)
    @Retry(maxRetries = 1)
    @Timeout(200)
    @Fallback(fallbackMethod = "defaultBook")
    public Book findByBookId(@PathParam("id") Long id){
        return bookRestClient.findByBookId(id);
    }

    public Book defaultBook(Long id){
        Book book = new Book();
        book.tittle = "default";
        book.author = "admin";
        return book;
    }


}
