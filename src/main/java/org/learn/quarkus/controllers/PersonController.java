package org.learn.quarkus.controllers;

import io.quarkus.panache.common.Sort;
import org.learn.quarkus.entities.Person;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

@Path("/person")
public class PersonController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Person> allPerson(){
        return Person.listAll(Sort.by("name"));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Map<String, ? extends Object> add(Person person){
        person.status = Person.Status.ACTIVE;
        person.persist();
        System.out.println(person.createdAt);
        return Map.of("status",person.isPersistent() ? Response.Status.CREATED : Response.Status.OK,"data",person);
    }

    @GET
    @Path("/{name}/{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public Person byNameAndEmail(@PathParam("name") String name, @PathParam("email") String email){
        System.out.println(name);
        System.out.println(email);
        return Person.findByNameAndEmail(name, email);
    }

    @GET
    @Path("/active")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Person> allActive(){
        return Person.allActivePerson();
    }
}
